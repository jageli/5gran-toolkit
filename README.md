# 5GRAN Toolkit

# 下载并使用

* 下载链接:[*https://gitlabe1.ext.net.nokia.com/j7li/5gran-toolkit*](https://gitlabe1.ext.net.nokia.com/j7li/5gran-toolkit)  
  ![](res/1.png)  

* 解压到本地电脑
* 打开Toolkit目录下Toolkit.exe  
  ![](res/2.png)

* 双击打开  
  首次打开可能需要30秒左右

* 主界面
  ![](res/3.png)

# 功能说明
## 1. 设置  
   点击设置按钮，进去全局设置界面  
   ![](res/4.png)    
   填写gNB ip地址 用户名 密码  
   APP server ip地址 用户名 密码  
   其他的暂时用不到  
   可以把配置信息取个名字，点击save按钮保存  
   下次打开只需要在choose下拉框里选择即可  
   配置好之后点击OK按钮  

## 2. Check Tput   
   软件启动后默认选择左边框Check Tput选项  
   软件底部会显示此软件的版本信息，连接的基站信息  
   点击RUN按钮开始检查基站的上下行吞吐量  
   手机接入5G 并获取手机的ip地址，填到UE ip的位置，并选择bandwidth(不是基站带宽，是下行的灌包大小)  
   在屏幕上就会有cell信息输出  
   ![](res/5.png)  
   左边是DL 右边是UL  

## 3. 曲线图      
   选择log文件  
   ![](res/6.png)    
   log默认存放位置 ...\Toolkit\log\Tputlog\ ...log    
   找到最新的log，点击curve按钮  
   ![](res/7.png)  
   可以设置peak值，并设置peak值的百分比  
   ![](res/8.png)    
   ![](res/9.png)   
   很明显可以看出图中UE的DL throughput低于峰值的90%, DL throughput 很接近峰值  
   如果图向不清晰，可以手动调整大小：点击图像上方的放大镜按钮调整，调整好后，点击十字按钮拖动。![](res/11.png)  
   ![](res/10.png)  

## 4. 其他功能  
### 计算小区峰值  
   > (服务器经常宕机，不太好用) :laughing:  
      
### 抓取bip log  
   > (还不完善)  :stuck_out_tongue_winking_eye:   
      
### 本地抓取BTS log  
   > * 需要本地安装BTSLog2 工具 
   > * 填写local PC ip (运行此软件的电脑的ip地址)
   > * 点击add Flag
   > * 使用BTSLog2工具抓取
   > * 抓取过程中，基站重启了，也可以继续抓，具体怎么操作联系作者，这个描述起来有点复杂 :stuck_out_tongue:  
   
### 取RRU log  
   > 需要填写登陆RRU的账号和密码

     
   